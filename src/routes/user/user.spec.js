const { app, server } = require('../../app');
const { expect } = require('chai');
const request = require('supertest')

const getRequest = (route) => {
    return new Promise((resolve, reject) => {
        request(app).get(route)
        .end((err, res) => {
            if (err){
                reject(err);
            }
            resolve(res);
        });
    })
}

describe('Test User APIs', () => {
    describe('GET/user/v1', () => {
        it('Should return hello message', (done) => {
            getRequest('/user/v1').then((res) => {
                expect(res.status).to.equal(200);
                expect(res.text).to.equal('hello from microservice!');
                done();
            })
            .catch(done);
        })
    });
    after(() => {
        server.close();
    })
})