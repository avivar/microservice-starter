const express = require('express');
const app = express();
const logger = require('modules/src/logger');
const { withRoutes } = require('modules/src/express');
const { catchExceptions } = require('modules/src/utils/exceptionHandler');
const usersRoute = require('./routes/user/user');

const port = process.env.PORT || '8000';
const microsrviceName = 'users';

catchExceptions(microsrviceName);
withRoutes(app, 'v1', 'user', usersRoute);

let server = app.listen(port, () => {
    logger.info(`${microsrviceName} microsrvice running in ${process.env.NODE_ENV || 'development'} mode; port= ${port}`);
})

module.exports.app = app;
module.exports.server = server;